package rk.musicplayer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by Toshiba on 2017-12-11.
 */

public class AlbumAdapter extends BaseAdapter {

    private Context context;

    public AlbumAdapter(Context context) {
        this.context = context;
    }

    public int getCount() {
        return albumList.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if (convertView == null) {
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(300, 300));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(10, 10, 10, 10);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(albumList[position]);
        return imageView;

    }

    private Integer[] albumList = {
            R.drawable.edsheeran,
            R.drawable.fever,
            R.drawable.linkin,
            R.drawable.pham,
            R.drawable.twofeet,
            R.drawable.witcher
    };

}
