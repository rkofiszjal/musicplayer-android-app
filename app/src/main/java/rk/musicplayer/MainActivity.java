package rk.musicplayer;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    MediaPlayer mediaPlayer;
    List<String> playList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GridView gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new AlbumAdapter(this));


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                if (mediaPlayer != null) {
                    mediaPlayer.release();
                }

                switch(position) {
                    case 0:
                        mediaPlayer = mediaPlayer.create(MainActivity.this, R.raw.edsheeran);
                        mediaPlayer.start();
                        Toast.makeText(MainActivity.this, "Ed Sheeran - I See Fire", Toast.LENGTH_LONG).show();
                        break;
                    case 1:
                        mediaPlayer = mediaPlayer.create(MainActivity.this, R.raw.fever);
                        mediaPlayer.start();
                        Toast.makeText(MainActivity.this, "Fever Ray - If I Had a Heart", Toast.LENGTH_LONG).show();
                        break;
                    case 2:
                        mediaPlayer = mediaPlayer.create(MainActivity.this, R.raw.linkin);
                        mediaPlayer.start();
                        Toast.makeText(MainActivity.this, "Linkin Park - From the Inside", Toast.LENGTH_LONG).show();
                        break;
                    case 3:
                        mediaPlayer = mediaPlayer.create(MainActivity.this, R.raw.pham);
                        mediaPlayer.start();
                        Toast.makeText(MainActivity.this, "Pham - Controls", Toast.LENGTH_LONG).show();
                        break;
                    case 4:
                        mediaPlayer = mediaPlayer.create(MainActivity.this, R.raw.twofeet);
                        mediaPlayer.start();
                        Toast.makeText(MainActivity.this, "Two Feet - Go F*ck Yourself", Toast.LENGTH_LONG).show();
                        break;
                    case 5:
                        mediaPlayer = mediaPlayer.create(MainActivity.this, R.raw.witcher);
                        mediaPlayer.start();
                        Toast.makeText(MainActivity.this, "The Witcher 3 - The Trail (ft. Percival)", Toast.LENGTH_LONG).show();
                        break;
                }

            }
        });
    }

}